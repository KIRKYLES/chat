from threading import Thread
from tkinter import *
import socket


class ListboxChat:

    def __init__(self, master):
        frame = Frame(master)
        frame.pack()

        self.listbox = Listbox(frame, width=100)
        self.listbox.pack(side=TOP, fill=Y)

    def insert_to_listbox(self, msg):
        self.listbox.insert(END, msg)


class InputChat:

    def __init__(self, master):
        frame = Frame(master)
        frame.pack()

        self.input_chat = Entry(master, width=50)
        self.input_chat.pack()

    def take_data_from_input(self):
        return self.input_chat.get()

    def clean_input(self):
        self.input_chat.delete(0, END)


class ButtonSend:

    def __init__(self, master, command, text):
        frame = Frame()
        frame.pack()

        self.button_command = Button(frame, command=command, text=text)
        self.button_command.bind('<Enter>')
        self.button_command.pack()


class MainChat:

    def __init__(self, master):
        self.server = self.connect_to_server((str(sys.argv[1]), int(sys.argv[2])))
        self.master = master
        self.frame = Frame(master)
        self.frame.pack()

        self.listbox = ListboxChat(self.frame)

        self.input_chat = InputChat(self.frame)

        self.send_button = ButtonSend(self.frame, self.send_data_to_server, text='Отправить')

    def connect_to_server(self, ip_port):
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_sock.connect(ip_port)
        return server_sock

    def take_data_from_server(self):
        while True:
            data = self.server.recv(1024)
            self.listbox.insert_to_listbox(data.decode())

    def send_data_to_server(self):
        data = self.input_chat.take_data_from_input()
        self.server.send(str.encode(data))
        self.input_chat.clean_input()

    def on_closing(self):
        self.server.send(bytes('-quit-', 'utf-8'))
        self.master.destroy()

    def start_chat(self):
        Thread(target=self.take_data_from_server).start()


if __name__ == '__main__':
    root = Tk()
    main = MainChat(root)
    main.start_chat()
    root.protocol('WM_DELETE_WINDOW', main.on_closing)
    root.mainloop()

import socket
import sys
import time

from fib import fib
from threading import Thread

print(sys.argv)
clients = []


def run_server():
    serv_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv_socket.bind((str(sys.argv[1]), int(sys.argv[2])))
    serv_socket.listen(10)
    while True:
        client, addr = serv_socket.accept()
        clients.append(client)
        print(addr)
        print(clients)
        Thread(target=client_handler, args=(client,)).start()


def client_handler(client):
    client.send(str.encode('Please enter your name:'))
    prefix = client.recv(1024)
    client.send(str.encode('Welcome to the chat, {0}!'.format(prefix.decode())))
    time.sleep(0.5)
    broadcast('{0} entered in chat!'.format(prefix.decode()))
    while True:
        data = client.recv(1024)
        if data != bytes('-quit-', 'utf-8'):
            broadcast('[{0}]: '.format(prefix.decode()) + data.decode())
        else:
            remove_client(client)
            broadcast('{0} left from chat!'.format(prefix.decode()))
            client.close()
            break


def remove_client(client):
    clients.remove(client)


def broadcast(msg):
    for sock in clients:
        sock.send(str.encode(msg))


run_server()
